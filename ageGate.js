
(function ($) {
    var AG_NAMESPACE = 'ageGate',
	ag_numInstances = 0;
    var curent_date = new Date();
	var year = curent_date.getFullYear();
    var defaults = {
		logo: 'http://www.provenancevineyards.com/assets/images/logo.jpg',
		lang: 'en',
		min_age: 21,
		age_span: 100,
		month_names: ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"]
    },
	langStrings = {
	    'en': {
	        welcome_text: 'To view the website you must be of legal drinking age within your country of residence. If no such laws exist in your country, you must be over 21 to visit our website.',
			underage_text: "Sorry, you do not meet the minimum age requirements",
			error_text: "Please enter country of residence and your birthday date",
			country_error: "Please select country of your residence",
			dob_error: "Please enter your birthday date"
		},
		'cn': {
	        welcome_text: '要查看网站，您必须在您居住的国家之内达到法定饮酒年龄。如果您所在的国家不存在这样的法律，你必须年满21访问我们的网站。',
			underage_text: "对不起，你不符合最低年龄要求",
			error_text: "请输入居住的国家和你的生日日期",
			country_error: "请选择您所在的国家",
			dob_error: "请输入您的生日日期"
	    }
	};			
	
	var methods = {
        /**
        * Initialization method for embedding Age Gate prompt
        * @param {Object} age_gate	Object containing all the age_gate for configuring Age Gate
        */

        init: function (age_gate) {
            // extend the default fields
            var age_gate = $.extend(defaults, age_gate);

            // build out each component (everything in here actually acts like our objects)
            return this.each(function () {
                // increase the number instances we have
                ag_numInstances++;

                var _target = $(this),
					_data = _target.data(AG_NAMESPACE),
					_me = this,
					_lang = age_gate.lang,
					_strings = langStrings[_lang];
					
				// there's data, then the component has probably been initialized already
                // we probably don't need to continue with the rest of initialization
                if (_data) return;
                //_target.html('').data(AG_NAMESPACE, age_gate).addClass('ageGate').width(age_gate.width).height(age_gate.height);
				
				function buildHTML() {

				
					// Generate static objects
					// Days selector
					var days = '<option value="" selected disabled>Day</option>';
					for (var i = 1; i < 32; i++) {
					  days += '<option value="' + i + '">' + i + '</option>';
					}
					var select_day = '<select name="age-day" class="age-dob form-control">' + days + '</select>';
					// Months selector
					var months = '<option value="" selected disabled>Month</option>';
					for (var i = 0; i < 12; i++) {
					  var month_value = i + 1;
					  months += '<option value="' + month_value + '">' + age_gate.month_names[i] + '</option>';
					}
					var select_month = '<select name="age-month" class="age-dob form-control">' + months + '</select>';
					// Years selector
					var years = '<option value="" selected disabled>Year</option>';
					for (var i = year - 8; i > (year - age_gate.age_span); i--) {
					  years += '<option value="' + i + '">' + i + '</option>';
					}
					var select_year = '<select name="age-year" class="age-dob form-control">' + years + '</select>';

					//build an overlay (css required)
					var age_gate_container = '<div id="age-gate"><div>'+
					  '<img src="'+ age_gate.logo +'" alt="Logo" class="age-gate-logo" />'+
					  _strings.welcome_text +
					  '<form id="age-verification-form">'+  
					  '<div class="form-item"><label for="edit-country" class="age-country">Country: </label><select name="country" id="age-country" class="age-country form-control"><option value="" selected="selected" disabled>Choose your Country</option><option value="US" selected>United States</option><option value="US">Canada</option><option value="AU">Australia</option><option value="UK">United Kingdom</option><option value="MX">Mexico</option><option value="US">Other</option></select></div>' +
					  '<div class="form-item"><label for="edit-dob" class="age-dob">Birthdate: </label><div class="form-inline">' +
					  select_month + select_day + select_year +
					  '</div></div><div id="age-error" class="error"></div><div class="form-item"><input type="submit" value="Enter"></div></form></div></div>';
					//end of static content
					  
					// If the age_gate cookie is set, use it.
					  var cookie_js = readCookie('ageVerified');
					//cookie_js = 'x';
					  if (cookie_js === 'no') {
						$("#age-error").text(_strings.underage_text);
					  } else if  (cookie_js === 'yes') {
						 console.log("Age is verified");
					  } else {
						$("body").append(age_gate_container);
					  };


					$("#age-verification-form").submit(function() {
					  cn = $('#age-country option:selected').val(),
						dob = $('select[name=age-year] option:selected').val() + '/' + $('select[name=age-month] option:selected').val() + '/' + $('select[name=age-day] option:selected').val(),
						e = 0,
						curent_date = new Date();

					  var dobb = new Date(dob);
					  var diff = Math.floor((curent_date - dobb) / (1000 * 60 * 60 * 24)); //days

					  $("#age-country-error, #age-dob-error").remove();
					  $(".age-country").removeClass("error");
					  
					  /* Check if country of residence is selected */
					  if (cn == '') {
						$(".age-country").append('<div class="error" id="age-country-error">' + _strings.country_error + '</div>');
						e++;
					  };

					  if (isNaN(diff) === true) {
						$(".age-dob").append('<div class="error" id="age-dob-error">' + _strings.dob_error +'</div>');
						
						e++;
					  } else if (diff < 21 * 365) {
						$(".age-dob").append('<div class="error" id="age-dob-error">' + _strings.underage_text + '</div>');
						e++;
					  };

					  if (e++ == 0) {
						createCookie('ageVerified','yes',1);
						$("#age-gate").fadeOut();
						return false;
					  } else {
						return false;
					  };
					return false;
					});				
				
				
				}
				// start the initialization
                buildHTML();
			});
        }, // end init
        destroy: function () {
            return this.each(function () {
                var $this = $(this),
					data = $this.data(AG_NAMESPACE);
                data[AG_NAMESPACE].remove();
                $this.removeData(AG_NAMESPACE);
            });
        } // end destroy 
    };
	createCookie = function (name, value, days) {
		var i = location.hostname.split("."),
			s = i.slice(-2).join(".");
		if (days) {
			var date = new Date();
			date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));
			var expires = "; expires=" + date.toGMTString();
		} else
			var expires = "";
		document.cookie = name + "=" + value + expires + "; path=/;domain=" + s;
	}
	eraseCookie = function (name) {
		createCookie(name, "", -1);
	}
	readCookie = function (name) {
        var nameEQ = name + "=";
		var ca = document.cookie.split(';');
		for(var i=0;i < ca.length;i++) {
			var c = ca[i];
			while (c.charAt(0)==' ') c = c.substring(1,c.length);
			if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length,c.length);
		}
		return null;
    };
	$.fn.ageGate = function (method) {
        if (methods[method]) {
            return methods[method].apply(this, Array.prototype.slice.call(arguments, 1));
        } else if (typeof method === 'object' || !method) {
            return methods.init.apply(this, arguments);
        } else {
            $.error('Method ' + method + ' does not exist on jQuery');
        }
    };
})(jQuery);
